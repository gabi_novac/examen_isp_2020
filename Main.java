package com.company;

//exercitiul 2

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.*;


class TextFieldExample implements ActionListener {
    JTextField tf1;
    JTextField tf3;
    JLabel label1;
    JLabel label2;
    JButton b1;

    TextFieldExample() {
        JFrame f = new JFrame("Problema Examen");
        f.setDefaultCloseOperation(3);
        this.tf1 = new JTextField();
        this.tf1.setBounds(50, 100, 350, 40);
        this.tf3 = new JTextField();
        this.tf3.setBounds(50, 200, 350, 40);
        this.tf3.setEditable(false);
        this.label1 = new JLabel("Introduceti textul dorit:");
        this.label1.setBounds(50, 175, 450, 20);
        this.label2 = new JLabel("Textul pe care l-ati introdus este:");
        this.label2.setBounds(50, 75, 450, 20);
        this.b1 = new JButton("Ok");
        this.b1.setBounds(50, 250, 100, 50);
        this.b1.addActionListener(this);
        f.add(this.tf1);
        f.add(this.tf3);
        f.add(this.b1);
        f.add(this.label1);
        f.add(this.label2);
        f.setSize(500, 400);
        f.setLayout((LayoutManager) null);
        f.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.b1) {
            this.tf3.setText(this.tf1.getText());
        }

    }

    public static void main(String[] args) {
        new TextFieldExample();
    }
}